# -*- coding: utf-8 -*-
"""
    consts
    ~~~~~~~~~~~~~~~
  

"""

JWT_IDENTITY_APP = 'jwt_identity_app'
JWT_PRIVATE_KEY = 'jwt_identity_private_key'
JWT_USER_DATA_DB_CLASS = 'jwt_user_data_db_class'
JWT_REFRESH_TOKEN_DB_CLASS = 'jwt_refresh_token_db_class'
