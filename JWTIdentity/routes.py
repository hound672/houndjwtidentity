# -*- coding: utf-8 -*-
"""
    routes
    ~~~~~~~~~~~~~~~
  

"""

from aiohttp import web

from JWTIdentity import apis


def init_routes(app_identity: web.Application) -> None:
    """
    Инициализирует роуты для подприлоежния app_identity
    :param app_identity:
    :return:
    """
    app_identity.add_routes([
        web.view('/login', apis.Login, name='login'),
        web.view('/refresh', apis.RefreshToken, name='refresh'),
        web.view('/logout', apis.Logout, name='logout')
    ])
