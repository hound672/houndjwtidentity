# -*- coding: utf-8 -*-
"""
    apis
    ~~~~~~~~~~~~~~~
  

"""

import json
import logging
from functools import wraps

from aiohttp import web
from aiohttp import web_exceptions

from JWTAuth import exceptions as jwt_auth_exceptions
from JWTAuth.helpers import DEBUG
from JWTAuth.mixins import JWTAuthMixin
from JWTAuth.structs import UserDataToken

from JWTIdentity import exceptions as jwt_identity_exceptions
from JWTIdentity.db import BaseUserDataDB, RefreshTokenBD
from JWTIdentity.consts import JWT_PRIVATE_KEY, JWT_IDENTITY_APP, \
    JWT_USER_DATA_DB_CLASS, JWT_REFRESH_TOKEN_DB_CLASS
from JWTIdentity.utils import generate_token, update_token, validate_password

logger = logging.getLogger(__name__)


def identity_app_decor(method):
    """
    Decorator for get identity_app, identity_polict, private_key from app
    and put it to requet View class
    also for manage exeptions
    """

    def identity_objects(self) -> None:
        try:

            self.request['identity_app'] = \
                self.request.config_dict[JWT_IDENTITY_APP]

            self.request['user_data_db_cls'] = \
                self.request['identity_app'][JWT_USER_DATA_DB_CLASS]

            self.request['refresh_token_db_cls'] = \
                self.request['identity_app'][JWT_REFRESH_TOKEN_DB_CLASS]

        except KeyError:  # pragma: no cover
            raise jwt_identity_exceptions.IdentityAppEmptyError

    def get_private_key(self) -> None:
        try:
            self.request['private_key'] = self.request['identity_app'][JWT_PRIVATE_KEY]
        except KeyError:  # pragma: no cover
            raise jwt_identity_exceptions.PrivateKeyEmptyError

    @wraps(method)
    async def wrapper(self):

        try:

            identity_objects(self)
            get_private_key(self)
            return await method(self)

        except jwt_auth_exceptions.InternalServerError as err:
            raise web_exceptions.HTTPInternalServerError(
                text=json.dumps({'error': str(err)}),
                content_type='application/json'
            )

        except jwt_identity_exceptions.IdentityFailed as err:
            raise web_exceptions.HTTPUnauthorized(
                text=json.dumps({'error': str(err)}),
                content_type='application/json'
            )

        except jwt_identity_exceptions.RefreshFailed as err:
            raise web_exceptions.HTTPUnauthorized(
                text=json.dumps({'error': str(err)}),
                content_type='application/json'
            )

        except jwt_identity_exceptions.LogoutFailed as err:
            raise web_exceptions.HTTPUnauthorized(
                text=json.dumps({'error': str(err)}),
                content_type='application/json'
            )

        except Exception as err:  # pragma: no cover
            logger.exception('Unknown error!')
            if DEBUG:
                _text = f'Something went wrong: {err}'
            else:
                _text = f'Something went wrong!'
            raise web_exceptions.HTTPInternalServerError(
                text=json.dumps({'error': _text}),
                content_type='application/json'
            )

    return wrapper


########################################################################
# TODO write for swagger doc for methods
class Login(web.View):
    """
    Endpoint for authenticate
    Steps for authenticate user:
    1. check its credentials, by login and password, exists in database etc
    2. create refresh token
    3. create access token with link to refresh token
        link for refresh token needs to a later refresh and logout
    """

    @identity_app_decor
    async def post(self):
        """
        ---
        description: This end-point for authenticate.
        tags:
        - Identity service
        produces:
        - application/json
        parameters:
        - in: body
          name: id   # Note the name is the same as in the path
          required: true
        responses:
            "200":
                description: successful operation. Return user token in JSON {"token":jwt}
            "401":
                description: invalid credentials. See details in answer JSON {"error":description}
        """

        user_data_db \
            = await self.identity_user(self.request)  # type: BaseUserDataDB

        refresh_token_db \
            = await self.create_refresh_token(user_data_db)  # type: RefreshTokenBD

        access_token \
            = await self.create_access_token(user_data_db, refresh_token_db)  # type: UserDataToken

        return web.json_response({
            'token': access_token
        })

    async def identity_user(self, request: web.Request) -> BaseUserDataDB:
        """
        Identity user
        Check does it exist. Valid password. Etc
        :return: BaseUserDataDB instance with user data
        """

        try:
            credentials = await request.json()  # type: dict
            login = credentials['login']
            password = credentials['password']

            # getting user from DB
            user_data_db_cls = self.request['user_data_db_cls']
            user_data_db = await user_data_db_cls.get_by_login(login=login)  # type: BaseUserDataDB

        except json.JSONDecodeError:
            raise jwt_identity_exceptions.IdentityFailedNoJson

        except KeyError:
            raise jwt_identity_exceptions.IdentityFailedNoUsernameOrPassword

        except BaseUserDataDB.UserDoesNotExist:
            raise jwt_identity_exceptions.IdentityFailedErrorCredentials

        # check password
        password_hash = user_data_db.password
        if not validate_password(password=password, password_hash=password_hash):
            raise jwt_identity_exceptions.IdentityFailedErrorCredentials

        return user_data_db

    async def create_refresh_token(self, user_data_db: BaseUserDataDB) -> RefreshTokenBD:
        """
        Create refresh token
        :return: refresh token's id
        """
        refresh_token_db_cls = self.request['refresh_token_db_cls']
        refresh_token_db \
            = await refresh_token_db_cls.create(user_data_db=user_data_db) # type: RefreshTokenBD
        return refresh_token_db

    async def create_access_token(self,
                                  user_data_db: BaseUserDataDB,
                                  refresh_token_db: RefreshTokenBD) -> str:
        """
        Create access token for user
        :return: JWT
        """
        private_key = self.request['private_key']
        token = generate_token(user_data_token=user_data_db.to_user_data_token(),
                               refresh_token=refresh_token_db.id,
                               private_key=private_key)  # type: str
        return token


########################################################################


class RefreshToken(JWTAuthMixin, web.View):
    """
    Endpoint for refresh token
    Steps for refresh token:
    1. Check does refresh token exist in database
    2. Update exp time in access token
    3. Send access token back
    """

    _verify_expired = False

    @identity_app_decor
    async def post(self):
        """
        ---
        description: This end-point for refresh access token.
        tags:
        - Identity service
        produces:
        - application/json
        responses:
            "200":
                description: successful operation. Return new access token in JSON like /login/ end-point.
            "401":
                description: invalid access token or refresh token on server.
        """

        user_data_token = self.request['user']  # type: UserDataToken
        refresh_token_db_cls = self.request['refresh_token_db_cls']

        try:
            refresh_token_db = await refresh_token_db_cls.get(
                id=user_data_token.jti,
                user_id=user_data_token.sub
            )  # type: RefreshTokenBD

            # TODO validate refresh_token
            # such validation as type client, maybe IP etc
            access_token = update_token(user_data_token=user_data_token,
                                        private_key=self.request['private_key'])

        except RefreshTokenBD.RefreshTokenDoesNotExist:
            raise jwt_identity_exceptions.RefreshFailedNoRefreshToken

        return web.json_response({
            'token': access_token
        })


########################################################################


class Logout(JWTAuthMixin, web.View):
    """
    Endpoint logout
    Steps for logout user:
    1. Delete refresh token form database
    Without refresh token user cannot do refresh
    """

    @identity_app_decor
    async def post(self):
        """
        ---
        description: This end-point for logout user from service.
        tags:
        - Identity service
        produces:
        - application/json
        responses:
            "200":
                description: successful operation. Return nothing.
            "401":
                description: invalid access token in header or its may be expired.
        """

        # try:
        #     await self.request['identity_policy'].delete_refresh_token(self.request['user'])
        # except AbstractIdentityPolicy.RefreshTokenDoesNotExist:
        #     raise jwt_identity_exceptions.LogoutFailedNoRefreshToken

        user_data_token = self.request['user']  # type: UserDataToken
        refresh_token_db_cls = self.request['refresh_token_db_cls']

        try:
            refresh_token_db = await refresh_token_db_cls.get(
                id=user_data_token.jti,
                user_id=user_data_token.sub
            )  # type: RefreshTokenBD

            await refresh_token_db.delete()

        except RefreshTokenBD.RefreshTokenDoesNotExist:
            raise jwt_identity_exceptions.RefreshFailedNoRefreshToken

        return web.Response(status=web_exceptions.HTTPOk.status_code)
