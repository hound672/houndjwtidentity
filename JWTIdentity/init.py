# -*- coding: utf-8 -*-
"""
    init
    ~~~~~~~~~~~~~~~
  

"""

from typing import Callable
from aiohttp import web

from JWTIdentity.consts import JWT_IDENTITY_APP, JWT_USER_DATA_DB_CLASS, \
    JWT_PRIVATE_KEY, JWT_REFRESH_TOKEN_DB_CLASS
from JWTIdentity.routes import init_routes


def init_identity(*, app: web.Application,
                  user_data_db_cls: Callable,
                  refresh_token_db_cls: Callable,
                  private_key: bytes) -> None:
    """
    Init nested application JWTIdentity
    """

    identity_app = web.Application()
    init_routes(identity_app)
    # TODO make custom field for sub_app

    identity_app[JWT_PRIVATE_KEY] = private_key
    identity_app[JWT_USER_DATA_DB_CLASS] = user_data_db_cls
    identity_app[JWT_REFRESH_TOKEN_DB_CLASS] = refresh_token_db_cls

    app[JWT_IDENTITY_APP] = identity_app

    app.add_subapp('/identity/', identity_app)
