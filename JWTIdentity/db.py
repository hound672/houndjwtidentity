# -*- coding: utf-8 -*-
"""
    db
    ~~~~~~~~~~~~~~~
  

"""

import abc
from typing import Union
from itertools import chain

from JWTAuth.structs import UserDataToken

from JWTIdentity.utils import generate_password_hash


class AbstractDBClient(metaclass=abc.ABCMeta):
    """
    Abstract class for access to database
    """

    @abc.abstractmethod
    async def select_user(self, _query: dict):
        """
        Get data from database.
        If there is no data returns None
        """
        pass  # pragma: no cover

    @abc.abstractmethod
    async def update_user(self, _query: dict):
        """
        Update data in database
        """
        pass  # pragma: no cover

    @abc.abstractmethod
    async def insert_user(self, _query: dict) -> Union[str, int]:
        """
        Insert data into database.
        Return ID inserted record
        """
        pass  # pragma: no cover

    @abc.abstractmethod
    async def delete_user(self, _query: dict):
        """
        Delete data from database
        """
        pass  # pragma: no cover

    @abc.abstractmethod
    async def insert_refresh_token(self, _query: dict) -> Union[str, int]:
        """
        Insert data into database.
        Return ID inserted record
        """
        pass  # pragma: no cover

    @abc.abstractmethod
    async def select_refresh_token(self, _query: dict):
        """
        Get data from database.
        If there is no data returns None
        """
        pass  # pragma: no cover

    @abc.abstractmethod
    async def delete_refresh_token(self, _query: dict):
        """
        Delete data from database
        """
        pass  # pragma: no cover



###################################################################################################


class BaseUserDataDB:
    """
    Base class for user store
    """

    class UserDoesNotExist(Exception):
        pass

    # list of common fields
    _base_fields = ['id', 'username', 'password', 'is_active', 'is_admin']
    # fields from child
    FIELDS_EXT = []
    FIELDS_TO_JWT = []
    # field for user log in
    USERNAME_FIELD = 'username'
    # DB client for access to database
    DB_CLIENT: AbstractDBClient = None

    _id: Union[str, int]  # primary key in database
    _password: str  # password hash
    username: str  # name of user
    is_active: bool  # flag is user active
    is_admin: bool  # flag is user admin

    def __init__(self, **kwargs) -> None:
        assert isinstance(self.DB_CLIENT, AbstractDBClient), 'DBClient is not AbstractDBClient type'

        self._id = None
        self.username = ''
        self._password = ''
        self.is_active = True
        self.is_admin = False

        for key, value in kwargs.items():
            if key == 'id':
                self._id = value
            elif key == 'password':
                self._password = value
            elif key in self._base_fields + self.FIELDS_EXT:
                setattr(self, key, value)

        super().__init__()

    @property
    def id(self):
        """
        Denied change directly
        """
        return self._id

    @property
    def password(self):
        """
        Denied change directly
        """
        return self._password

    def set_password(self, raw_password: str):
        self._password = generate_password_hash(password=raw_password)

    async def save(self):
        data = {}
        for field in chain(self._base_fields, self.FIELDS_EXT):
            data[field] = getattr(self, field, None)

        if self._id is None:
            self._id = await self.DB_CLIENT.insert_user(data)
        else:
            self._id = await self.DB_CLIENT.update_user(data)

        return self._id

    @classmethod
    async def get(cls, **kwargs):
        """
        Looks for user in database and returns it
        If the user is not found, return none
        """
        query = {**kwargs}
        res = await cls.DB_CLIENT.select_user(query)
        if res is None:
            raise BaseUserDataDB.UserDoesNotExist

        return cls(**res)

    @classmethod
    async def get_by_login(cls, login):
        """
        Looks user by login field which set up in settings
        """
        return await cls.get(**{cls.USERNAME_FIELD: login})

    def to_user_data_token(self) -> UserDataToken:
        """
        Convert user data to UserDataToken
        :return:
        """
        return UserDataToken({
            'sub': self.id,
            'jti': None,
            'exp': None,
            **{key: getattr(self, key, None) for key in self.FIELDS_TO_JWT}
        })


###################################################################################################


class RefreshTokenBD:
    """
    Class for processing with refresh tokens in database
    """

    class RefreshTokenDoesNotExist(Exception):
        pass

    DB_CLIENT: AbstractDBClient = None
    # list of common fields
    _base_fields = ['id', 'user_id']

    _id: Union[str, int]
    user_id: Union[str, int]

    def __init__(self, **kwargs) -> None:
        assert isinstance(self.DB_CLIENT, AbstractDBClient), 'DBClient is not AbstractDBClient type'

        self._id = None
        self.user_id = None

        for key, value in kwargs.items():
            if key == 'id':
                self._id = value
            elif key in self._base_fields:
                setattr(self, key, value)

        super().__init__()

    @property
    def id(self):
        """
        Denied change directly
        """
        return self._id

    @classmethod
    async def create(cls, user_data_db: BaseUserDataDB, **kwargs):
        """
        Create refresh token in database
        """
        query = {
            'user_id': user_data_db.id
            # TODO other data
        }
        id = await cls.DB_CLIENT.insert_refresh_token(query)
        query.update({'id': id})
        return cls(**query)

    @classmethod
    async def get(cls, **kwargs):
        """
        Looks for user in database and returns it
        If user did not find would return None
        """
        query = {**kwargs}
        res = await cls.DB_CLIENT.select_refresh_token(query)
        if res is None:
            raise RefreshTokenBD.RefreshTokenDoesNotExist

        return cls(**res)

    async def delete(self):
        query = {
            'id': self._id,
            'user_id': self.user_id
        }
        await self.DB_CLIENT.delete_refresh_token(query)
