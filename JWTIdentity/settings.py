# -*- coding: utf-8 -*-
"""
    settings
    ~~~~~~~~~~~~~~~
  

"""

import datetime

from JWTAuth.helpers import import_from_string

try:
    import settings
except ImportError:  # pragma: no cover
    raise RuntimeError('Cannot import settings!')

# living time of access token
JWT_IDENTITY_EXP_TIME = getattr(settings, 'JWT_IDENTITY_EXP_TIME', datetime.timedelta(seconds=3000))
