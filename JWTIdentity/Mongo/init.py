# -*- coding: utf-8 -*-
"""
    init_mongo
    ~~~~~~~~~~~~~~~
  

"""

from typing import Callable
from bson import ObjectId

from JWTIdentity.db import AbstractDBClient, RefreshTokenBD


class _MongoDBClient(AbstractDBClient):
    """
    Class client to MongoDB for IdentityApp
    """
    REFRESH_TOKEN_COLLECTION_NAME = 'refresh_tokens'

    def __init__(self, mongo_client, user_collection_name):
        self._db = mongo_client
        self._users = mongo_client[user_collection_name]  # user collection
        self._refreshes = mongo_client[_MongoDBClient.REFRESH_TOKEN_COLLECTION_NAME]

    async def select_user(self, _query: dict):
        query = {**_query}
        if 'id' in _query:
            query['_id'] = ObjectId(query.pop('id'))

        res = await self._users.find_one(query)
        if res is not None:
            res['id'] = str(res.pop('_id'))

        return res

    async def update_user(self, _query: dict):
        # TODO доделать с методом UPDATE
        pass

    async def insert_user(self, _query: dict):
        query = {**_query}
        query.pop('id', None)
        res = await self._users.insert_one(query)
        return str(res.inserted_id)

    async def delete_user(self, _query: dict):
        pass

    async def insert_refresh_token(self, _query: dict):
        query = {**_query}
        query.pop('id', None)
        query['user_id'] = ObjectId(query['user_id'])
        res = await self._refreshes.insert_one(query)
        return str(res.inserted_id)

    async def select_refresh_token(self, _query: dict):
        query = {**_query}
        if 'id' in _query:
            query['_id'] = ObjectId(query.pop('id'))
        if 'user_id' in _query:
            query['user_id'] = ObjectId(query.pop('user_id'))

        res = await self._refreshes.find_one(query)
        if res is not None:
            res['id'] = str(res.pop('_id'))
            res['user_id'] = str(res.pop('user_id'))

        return res

    async def delete_refresh_token(self, _query: dict):
        query = {**_query}
        if 'id' in _query:
            query['_id'] = ObjectId(query.pop('id'))
        if 'user_id' in _query:
            query['user_id'] = ObjectId(query.pop('user_id'))

        # TODO возможно добавить проверку на наличие токена
        await self._refreshes.delete_one(query)


# TODO попробовать потом не возвращать класс, а прописывать его в параметре ф-ции
async def init_mongo(*, mongo_client,
                     user_data_db_cls: Callable):
    """
    Init mongo
    :return Class for RefreshTokenDB
    """
    _client = _MongoDBClient(mongo_client, user_data_db_cls.__name__)
    # NOTE `a little hack just change DB_CLIENT in our class
    user_data_db_cls.DB_CLIENT = _client

    refresh_token_db_cls = RefreshTokenBD
    refresh_token_db_cls.DB_CLIENT = _client

    return refresh_token_db_cls
