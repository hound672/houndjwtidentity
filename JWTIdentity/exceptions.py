# -*- coding: utf-8 -*-
"""
    exceptions
    ~~~~~~~~~~~~~~~
  

"""

from JWTAuth import exceptions as jwt_auth_exceptions


class PrivateKeyEmptyError(jwt_auth_exceptions.InternalServerError):
    _detail = 'Private key is empty!'


class IdentityAppEmptyError(jwt_auth_exceptions.InternalServerError):
    _detail = 'There is no identity app!'


class IdentityFailed(jwt_auth_exceptions.BaseException):
    _detail = 'Incorrect identity credentials.'


class IdentityFailedNoJson(IdentityFailed):
    _detail = 'There is no valid JSON in POST request.'


class IdentityFailedNoUsernameOrPassword(IdentityFailed):
    _detail = 'Username/password not found.'


class IdentityFailedErrorCredentials(IdentityFailed):
    _detail = 'Invalid username/password combination.'


class RefreshFailed(jwt_auth_exceptions.BaseException):
    _detail = 'Refresh failed'


class RefreshFailedNoRefreshToken(RefreshFailed):
    _detail = 'Refresh. There is no refresh token for this user'


class LogoutFailed(jwt_auth_exceptions.BaseException):
    _detail = 'Logout failed'


class LogoutFailedNoRefreshToken(LogoutFailed):
    _detail = 'Logout. There is no refresh token for this user'
