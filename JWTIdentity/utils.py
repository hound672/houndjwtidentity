# -*- coding: utf-8 -*-
"""
    utils
    ~~~~~~~~~~~~~~~


"""

import jwt
from typing import Union
from datetime import datetime
from passlib.hash import pbkdf2_sha256

from JWTAuth.structs import UserDataToken

from JWTIdentity import settings as jwt_identity_settings


def get_expire_token_time():
    """
    Calculate exp token time
    """
    return datetime.utcnow() + jwt_identity_settings.JWT_IDENTITY_EXP_TIME


def validate_password(*, password, password_hash) -> bool:
    """
    Проводит валидацию пароля с его хешем
    :return: True: если пароль верный, иначе False
    """
    try:
        res = pbkdf2_sha256.verify(password, password_hash)
    except ValueError:
        return False

    return res


def generate_password_hash(*, password) -> str:
    """
    Generate hash of password
    :param password: raw password
    :return: password's hash
    """
    return pbkdf2_sha256.hash(password)


def generate_token(*, user_data_token: UserDataToken,
                   refresh_token: Union[int, str],
                   private_key: str) -> str:
    """
    Generate JWT from user data
    :param user_data_token: UserDataToken
    :param refresh_token: refresh_token's id which stored in database
    :param private_key: private_key for sign
    :return:
    """

    user_data_token.jti = refresh_token
    user_data_token.exp = get_expire_token_time()

    return encode_token(user_data_token=user_data_token, private_key=private_key)


def update_token(*, user_data_token: UserDataToken, private_key) -> str:
    """
    Update token
    """
    user_data_token.exp = get_expire_token_time()
    return encode_token(user_data_token=user_data_token,
                        private_key=private_key)


def encode_token(*,
                 user_data_token: UserDataToken,
                 private_key: str) -> str:
    """
    Encode token
    :return:
    """

    token = jwt.encode(
        payload=user_data_token.to_dict(),
        key=private_key,
        algorithm='RS256'
    )
    return token.decode('utf-8')
