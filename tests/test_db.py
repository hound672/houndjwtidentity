# -*- coding: utf-8 -*-
"""
    test_db
    ~~~~~~~~~~~~~~~
  

"""

import pytest

from JWTIdentity.db import BaseUserDataDB, AbstractDBClient, RefreshTokenBD
from JWTIdentity.utils import validate_password


class ClientDB(AbstractDBClient):
    _store = []
    _last_id: int = 0

    @property
    def store(self):
        return self._store

    async def select_user(self, query: dict):
        pass

    async def update_user(self, query: dict):
        pass

    async def insert_user(self, query: dict):
        self._last_id += 1
        query.update({'id': self._last_id})
        self._store.append(query)
        return self._last_id

    async def delete_user(self, query: dict):
        pass

    async def insert_refresh_token(self, _query: dict):
        pass

    async def select_refresh_token(self, _query: dict):
        pass

    async def delete_refresh_token(self, _query: dict):
        pass

@pytest.fixture
def UserDataDB():
    client_db = ClientDB()

    class _UserDataDB(BaseUserDataDB):
        DB_CLIENT = client_db
        FIELDS_EXT = ['new_field', 'new_field_2']
        FIELDS_TO_JWT = ['new_field']
        new_field: str
        new_field_2: str

    return _UserDataDB


@pytest.fixture
def RefreshTokenDB():
    client_db = ClientDB()
    refresh_token_db_cls = RefreshTokenBD
    refresh_token_db_cls.DB_CLIENT = client_db
    return refresh_token_db_cls


def test_base_user_data_db_fail_wo_db_client():
    class TmpClass(BaseUserDataDB):
        pass

    with pytest.raises(AssertionError):
        obj = TmpClass()


def test_base_user_data_db_fail_wrong_type_db_client():
    class TmpClass(BaseUserDataDB):
        DB_CLIENT = 123

    with pytest.raises(AssertionError):
        obj = TmpClass()


def test_base_user_data_db_success_create_object(UserDataDB):
    obj = UserDataDB()  # type: BaseUserDataDB
    store = obj.DB_CLIENT  # type: ClientDB

    # check if store is empty (just internal checking)
    assert not store.store


def test_base_user_data_db_set_password(UserDataDB, faker):
    user_data_db = UserDataDB()

    password = faker.password()
    user_data_db.set_password(password)

    assert validate_password(password=password, password_hash=user_data_db.password)


async def test_base_save_update_user(UserDataDB, faker):
    user = UserDataDB()  # type: BaseUserDataDB
    store = UserDataDB.DB_CLIENT  # type: ClientDB

    _username = faker.user_name()
    _password = faker.password()
    _is_active = False
    _is_admin = True

    assert user.id is None

    user.set_password(_password)
    user.username = _username
    user.is_active = _is_active
    user.is_admin = _is_admin
    user_id = await user.save()

    # user_from_db
    for u in store.store:
        if u['id'] == user_id:
            user_from_db = u
            break
    else:
        raise AssertionError('User did not found in database after insert')

    assert user_from_db['id'] == user_id
    assert user_from_db['username'] == _username
    assert user_from_db['is_active'] == _is_active
    assert user_from_db['is_admin'] == _is_admin
    assert validate_password(password=_password, password_hash=user_from_db['password'])


def test_base_user_data_db_from_dict(UserDataDB, faker):
    user_data = {
        'id': faker.random_int(),
        'username': faker.user_name(),
        'password': faker.password(),
        'is_active': False,
        'is_admin': True,
        'new_field': faker.word()
    }

    user = UserDataDB(**user_data)  # type: BaseUserDataDB

    assert user.id == user_data['id']
    assert user.username == user_data['username']
    assert user.password == user_data['password']
    assert user.is_active == user_data['is_active']
    assert user.is_admin == user_data['is_admin']
    assert user.new_field == user_data['new_field']


def test_base_user_data_db_to_jwt(UserDataDB, faker):
    user_data = {
        'id': faker.random_int(),
        'username': faker.user_name(),
        'password': faker.password(),
        'is_active': False,
        'is_admin': True,
        'new_field': faker.word(),
        'new_field_2': faker.word()
    }

    user = UserDataDB(**user_data)  # type: BaseUserDataDB
    user_data_token = user.to_user_data_token()

    assert user_data_token.sub == user_data['id']
    assert user_data_token.exp is None
    assert user_data_token.jti is None
    assert user_data_token.get('new_field') == user_data['new_field']
    assert user_data_token.get('new_field_2') is None


def test_base_refresh_token_db_from_dict(RefreshTokenDB, faker):
    refresh_data = {
        'id': faker.random_int(),
        'user_id': faker.random_int(),
    }

    refresh_token = RefreshTokenBD(**refresh_data)  # type: RefreshTokenDB

    assert refresh_token.id == refresh_data['id']
    assert refresh_token.user_id == refresh_data['user_id']
