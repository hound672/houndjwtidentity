# -*- coding: utf-8 -*-
"""
    test_urls
    ~~~~~~~~~~~~~~~
  

"""


def test_url_login(mongo_app, get_identity_app):
    identity_app = get_identity_app(mongo_app)
    assert str(identity_app.router['login'].url_for()) == '/identity/login'


def test_url_logout(mongo_app, get_identity_app):
    identity_app = get_identity_app(mongo_app)
    assert str(identity_app.router['logout'].url_for()) == '/identity/logout'


def test_url_refresh(mongo_app, get_identity_app):
    identity_app = get_identity_app(mongo_app)
    assert str(identity_app.router['refresh'].url_for()) == '/identity/refresh'
