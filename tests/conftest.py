# -*- coding: utf-8 -*-
"""
    conftest
    ~~~~~~~~~~~~~~~
  

"""

import os
import pytest

from faker import Factory

from JWTIdentity.consts import JWT_IDENTITY_APP

pytest_plugins = ['mongo.mongo_tester']


def key_path(key_name) -> object:
    return os.path.join(os.path.dirname(os.path.realpath(__file__)),
                        'keys', key_name)


@pytest.fixture
def faker():
    """
    Create faker object
    """
    return Factory.create()


@pytest.fixture
def public_key():
    """
    Read public key from file
    """
    with open(key_path('testkey.pub'), 'rb') as key_file:
        return key_file.read()


@pytest.fixture
def private_key():
    """
    Read private key from file
    """
    with open(key_path('testkey.pem'), 'rb') as key_file:
        return key_file.read()


@pytest.fixture
def get_identity_app():
    """
    Return object of identity app
    """
    return lambda app: app[JWT_IDENTITY_APP]


@pytest.fixture
async def get_url(get_identity_app):
    """
    Return str of url
    """

    def get_url(app, url):
        identity_app = get_identity_app(app)
        return str(identity_app.router[url].url_for())

    return get_url
