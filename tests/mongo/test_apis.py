# -*- coding: utf-8 -*-
"""
    test_apis
    ~~~~~~~~~~~~~~~
  

"""

import pytest
import time
from datetime import datetime
from calendar import timegm
from aiohttp import web, web_exceptions
from bson import ObjectId

from JWTAuth import init_auth
from JWTAuth import settings as jwt_auth_settings
from JWTAuth.utils import validate_token
from JWTAuth.structs import UserDataToken

from JWTIdentity import init_identity
from JWTIdentity import exceptions as jwt_identity_exceptions
from JWTIdentity import settings as jwt_identity_settings
from JWTIdentity.consts import JWT_IDENTITY_APP, JWT_USER_DATA_DB_CLASS
from JWTIdentity.db import BaseUserDataDB
from JWTIdentity.Mongo import init_mongo
from JWTIdentity.Mongo.init import _MongoDBClient


class UserDataTestMongoDB(BaseUserDataDB):
    FIELDS_EXT = ['some_field', 'another_one']
    FIELDS_TO_JWT = ['some_field']

    some_field: str
    another_one: str


USER_COLLECTION_NAME = UserDataTestMongoDB.__name__
REFRESH_TOKEN_COLLECTION_NAME = _MongoDBClient.REFRESH_TOKEN_COLLECTION_NAME


@pytest.fixture
async def mongo_app(mongo_client, private_key, public_key):
    app = web.Application()

    refresh_token_db_cls = await init_mongo(mongo_client=mongo_client,
                                            user_data_db_cls=UserDataTestMongoDB)
    init_identity(app=app,
                  user_data_db_cls=UserDataTestMongoDB,
                  refresh_token_db_cls=refresh_token_db_cls,
                  private_key=private_key)

    init_auth(app=app, public_key=public_key)

    await mongo_client[USER_COLLECTION_NAME].drop()
    await mongo_client[REFRESH_TOKEN_COLLECTION_NAME].drop()

    yield app

    await mongo_client[USER_COLLECTION_NAME].drop()
    await mongo_client[REFRESH_TOKEN_COLLECTION_NAME].drop()


@pytest.fixture
async def api_client(mongo_app, aiohttp_client):
    return await aiohttp_client(mongo_app)


@pytest.fixture
async def api_client_post(api_client):
    async def _request(url, json, token=None):
        header = {'Authorization': f'{jwt_auth_settings.JWT_AUTH_HEADER_PREFIX} {token}'}
        res = await api_client.post(url, json=json, headers=header)
        status_code = res.status
        try:
            answer = await res.json()
        except Exception:
            answer = await res.text()
        return status_code, answer

    return _request


@pytest.fixture
async def create_user(faker, mongo_app):
    """
    Create user for unittests
    """

    user_data_db_cls = mongo_app[JWT_IDENTITY_APP][JWT_USER_DATA_DB_CLASS]

    async def _creator():
        user = user_data_db_cls()  # type: UserDataTestMongoDB
        _password = faker.password()

        user.set_password(_password)
        user.username = faker.user_name()
        user.is_active = False
        user.is_admin = True
        user.some_field = faker.word()
        user.another_one = faker.word()

        await user.save()
        return user, _password

    return _creator


async def test_login_fail_empty_credentials(get_url, mongo_app, create_user, api_client_post, faker):
    url = get_url(mongo_app, 'login')
    await create_user()

    status_code, answer = await api_client_post(url, json={})

    assert status_code == web_exceptions.HTTPUnauthorized.status_code
    assert answer.get('error') == jwt_identity_exceptions.IdentityFailedNoUsernameOrPassword.get_detail()


async def test_login_fail_wrong_password(get_url, mongo_app, create_user, api_client_post, faker):
    url = get_url(mongo_app, 'login')
    user, password = await create_user()

    status_code, answer = await api_client_post(url, json={
        'login': user.username,
        'password': faker.password()
    })

    assert status_code == web_exceptions.HTTPUnauthorized.status_code
    assert answer.get('error') == jwt_identity_exceptions.IdentityFailedErrorCredentials.get_detail()


async def test_login_fail_wrong_username(get_url, mongo_app, create_user, api_client_post, faker):
    url = get_url(mongo_app, 'login')
    user, password = await create_user()

    status_code, answer = await api_client_post(url, json={
        'login': faker.user_name(),
        'password': password
    })

    assert status_code == web_exceptions.HTTPUnauthorized.status_code
    assert answer.get('error') == jwt_identity_exceptions.IdentityFailedErrorCredentials.get_detail()


async def test_login_success(get_url, mongo_app, create_user,
                             api_client_post, public_key, mongo_client):
    url = get_url(mongo_app, 'login')
    user, password = await create_user()

    status_code, answer = await api_client_post(url, json={
        'login': user.username,
        'password': password
    })

    assert status_code == web_exceptions.HTTPOk.status_code
    token = answer.get('token')

    user_data_token = validate_token(token=token, public_key=public_key)  # type: UserDataToken
    token_lifetime = datetime.utcnow() + jwt_identity_settings.JWT_IDENTITY_EXP_TIME

    refresh_token_from_db = mongo_client[REFRESH_TOKEN_COLLECTION_NAME].find_one({
        '_id': user_data_token.jti,
        'user_id': user_data_token.sub
    })

    assert refresh_token_from_db is not None
    assert user_data_token.sub == user.id
    assert user_data_token.exp == timegm(token_lifetime.utctimetuple())
    assert user_data_token.get('some_field') == user.some_field
    assert user_data_token.get('another_one') is None


async def test_login_refresh_success(get_url, mongo_app, create_user,
                                     api_client_post, public_key):
    url_login = get_url(mongo_app, 'login')
    url_refresh = get_url(mongo_app, 'refresh')
    user, password = await create_user()

    status_code, answer = await api_client_post(url_login, json={
        'login': user.username,
        'password': password
    })

    assert status_code == web_exceptions.HTTPOk.status_code

    token = answer.get('token')
    user_data_token = validate_token(token=token, public_key=public_key)
    exp_time_1 = user_data_token.exp

    time.sleep(1)

    status_code, answer = await api_client_post(url_refresh, json={}, token=token)

    assert status_code == web_exceptions.HTTPOk.status_code
    token = answer.get('token')
    user_data_token = validate_token(token=token, public_key=public_key)
    exp_time_2 = user_data_token.exp

    assert exp_time_2 == exp_time_1 + 1  # we slept a second
    assert user_data_token.sub == user.id
    assert user_data_token.get('some_field') == user.some_field
    assert user_data_token.get('another_one') is None


async def test_refresh_no_refresh_token(get_url, mongo_app, create_user,
                                         api_client_post, public_key, mongo_client):
        url_login = get_url(mongo_app, 'login')
        url_refresh = get_url(mongo_app, 'refresh')
        user, password = await create_user()

        status_code, answer = await api_client_post(url_login, json={
            'login': user.username,
            'password': password
        })

        assert status_code == web_exceptions.HTTPOk.status_code

        token = answer.get('token')
        user_data_token = validate_token(token=token, public_key=public_key)

        await mongo_client[REFRESH_TOKEN_COLLECTION_NAME].delete_one({
            '_id': ObjectId(user_data_token.jti),
            'user_id': ObjectId(user_data_token.sub)
        })

        status_code, answer = await api_client_post(url_refresh, json={}, token=token)
        assert status_code == web_exceptions.HTTPUnauthorized.status_code
        assert answer.get('error') == jwt_identity_exceptions.RefreshFailedNoRefreshToken.get_detail()


async def test_logout_success(get_url, mongo_app, create_user,
                                     api_client_post, public_key, mongo_client):
    url_login = get_url(mongo_app, 'login')
    url_logout = get_url(mongo_app, 'logout')
    user, password = await create_user()

    status_code, answer = await api_client_post(url_login, json={
        'login': user.username,
        'password': password
    })

    assert status_code == web_exceptions.HTTPOk.status_code
    token = answer.get('token')

    user_data_token = validate_token(token=token, public_key=public_key)

    status_code, answer = await api_client_post(url_logout, json={}, token=token)
    assert status_code == web_exceptions.HTTPOk.status_code

    data = await mongo_client[REFRESH_TOKEN_COLLECTION_NAME].find_one({
        '_id': ObjectId(user_data_token.jti),
        'user_id': ObjectId(user_data_token.sub)
    })

    assert data is None  # there is no refresh token in database because we deleted it
