# -*- coding: utf-8 -*-
"""
    test_mongo_db
    ~~~~~~~~~~~~~~~
  

"""

import random
import pytest
from bson import ObjectId

from JWTIdentity.db import BaseUserDataDB, RefreshTokenBD
from JWTIdentity.Mongo import init_mongo
from JWTIdentity.Mongo.init import _MongoDBClient
from JWTIdentity.utils import validate_password


class UserDataMongoTestDB(BaseUserDataDB):
    FIELDS_EXT = ['some_field']
    some_field: str


USER_COLLECTION_NAME = UserDataMongoTestDB.__name__
REFRESH_TOKEN_COLLECTION_NAME = _MongoDBClient.REFRESH_TOKEN_COLLECTION_NAME


@pytest.fixture
async def user_data_db_cls(mongo_client):
    await init_mongo(
        mongo_client=mongo_client,
        user_data_db_cls=UserDataMongoTestDB
    )

    await mongo_client[USER_COLLECTION_NAME].drop()

    yield UserDataMongoTestDB

    await mongo_client[USER_COLLECTION_NAME].drop()


@pytest.fixture
async def refresh_token_db_cls(mongo_client):
    refresh_token_db_cls = await init_mongo(
        mongo_client=mongo_client,
        user_data_db_cls=UserDataMongoTestDB
    )

    await mongo_client[REFRESH_TOKEN_COLLECTION_NAME].drop()

    yield refresh_token_db_cls

    await mongo_client[REFRESH_TOKEN_COLLECTION_NAME].drop()


@pytest.fixture
async def create_user(faker, user_data_db_cls):
    """
    Create user for unittests
    """

    async def _creator():
        user = user_data_db_cls()  # type: UserDataMongoTestDB
        _password = faker.password()

        user.set_password(_password)
        user.username = faker.user_name()
        user.is_active = False
        user.is_admin = True
        user.some_field = faker.word()

        await user.save()
        return user

    return _creator


@pytest.fixture
async def create_refresh_token(refresh_token_db_cls):
    async def _creator(user: UserDataMongoTestDB):
        refresh_token_db = await refresh_token_db_cls.create(user_data_db=user)
        return refresh_token_db

    return _creator


async def test_create_object(user_data_db_cls):
    obj = user_data_db_cls()


async def test_save_new_user(user_data_db_cls, faker, mongo_client):
    user = user_data_db_cls()  # type: UserDataMongoTestDB

    _username = faker.user_name()
    _password = faker.password()
    _is_active = False
    _is_admin = True
    _some_field = faker.word()

    assert user.id is None

    user.set_password(_password)
    user.username = _username
    user.is_active = _is_active
    user.is_admin = _is_admin
    user.some_field = _some_field
    user_id = await user.save()

    assert user_id is not None

    user_from_db = await mongo_client[USER_COLLECTION_NAME].find_one({
        '_id': ObjectId(user_id)
    })

    assert user_from_db is not None
    assert str(user_from_db['_id']) == user_id
    assert user_from_db['username'] == _username
    assert user_from_db['is_active'] == _is_active
    assert user_from_db['is_admin'] == _is_admin
    assert user_from_db['some_field'] == _some_field
    assert validate_password(password=_password, password_hash=user_from_db['password'])


async def test_save_existed_user(create_user, faker, mongo_client):
    user = await create_user()  # type: UserDataMongoTestDB

    # TODO доделать с методом UPDATE


async def test_get_user(create_user, mongo_client, user_data_db_cls: UserDataMongoTestDB, faker):
    user_collections = mongo_client[USER_COLLECTION_NAME]
    _cnt = 10
    users = []

    for i in range(_cnt):
        users.append(await create_user())

    def random_user():
        return users[random.randint(0, _cnt - 1)]

    # select by id
    user = random_user()

    user_from_db = await user_data_db_cls.get(id=user.id)
    assert user_from_db.id == user.id
    assert user_from_db.username == user.username
    assert user_from_db.password == user.password
    assert user_from_db.is_active == user.is_active
    assert user_from_db.is_admin == user.is_admin
    assert user_from_db.some_field == user.some_field

    # select by username
    user = random_user()

    user_from_db = await user_data_db_cls.get(username=user.username)
    assert user_from_db.id == user.id
    assert user_from_db.username == user.username
    assert user_from_db.password == user.password
    assert user_from_db.is_active == user.is_active
    assert user_from_db.is_admin == user.is_admin
    assert user_from_db.some_field == user.some_field

    # select by two fields
    user = random_user()

    user_from_db = await user_data_db_cls.get(username=user.username,
                                              id=user.id)
    assert user_from_db.id == user.id
    assert user_from_db.username == user.username
    assert user_from_db.password == user.password
    assert user_from_db.is_active == user.is_active
    assert user_from_db.is_admin == user.is_admin
    assert user_from_db.some_field == user.some_field

    # fail select

    with pytest.raises(BaseUserDataDB.UserDoesNotExist):
        await user_data_db_cls.get(username=faker.word())

    user = random_user()
    with pytest.raises(BaseUserDataDB.UserDoesNotExist):
        await user_data_db_cls.get(username=faker.word(),
                                   id=user.id)


async def test_create_refresh_token_sucess(refresh_token_db_cls, create_user, mongo_client):
    user = await create_user()  # type: UserDataMongoTestDB
    refresh_token_db = await refresh_token_db_cls.create(user)  # type: RefreshTokenBD

    refresh_token_from_db = await mongo_client[REFRESH_TOKEN_COLLECTION_NAME].find_one({
        '_id': ObjectId(refresh_token_db.id)
    })

    assert refresh_token_from_db is not None
    assert refresh_token_from_db['_id'] == ObjectId(refresh_token_db.id)
    assert refresh_token_from_db['user_id'] == ObjectId(refresh_token_db.user_id)
    assert refresh_token_db.user_id == user.id


async def test_get_refresh_token(create_user, create_refresh_token, refresh_token_db_cls, faker):
    user = await create_user()  # type: UserDataMongoTestDB
    refresh_token = await create_refresh_token(user)

    refresh_token_db = await refresh_token_db_cls.get(id=refresh_token.id,
                                                      user_id=refresh_token.user_id)

    assert refresh_token_db.id == refresh_token.id
    assert refresh_token_db.user_id == refresh_token.user_id == user.id

    # fail select
    user_fake = await create_user()
    with pytest.raises(RefreshTokenBD.RefreshTokenDoesNotExist):
        await refresh_token_db_cls.get(id=user_fake.id)
