# -*- coding: utf-8 -*-
"""
    mongo_tester
    ~~~~~~~~~~~~~~~
  
    Fixtures for MongoJWTIdentity Unittests
"""

import pytest
from motor import motor_asyncio as aiomotor

db_uri = 'mongodb://hound:12345@127.0.0.1/?authSource=Identity'
db_name = 'Identity'


@pytest.fixture
async def mongo_client(loop):
    """
    Create mongo client
    """
    db = aiomotor.AsyncIOMotorClient(db_uri)
    return db[db_name]


