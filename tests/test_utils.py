# -*- coding: utf-8 -*-
"""
    test_utls
    ~~~~~~~~~~~~~~~
  

"""

import pytest
from datetime import datetime
from calendar import timegm
from passlib.hash import pbkdf2_sha256

from JWTAuth import exceptions as jwt_auth_exceptions
from JWTAuth.utils import validate_token
from JWTAuth.structs import UserDataToken

from JWTIdentity import settings as jwt_identity_settings
from JWTIdentity.utils import validate_password, generate_password_hash, generate_token, update_token


def test_validate_password_error_password(faker):
    password = faker.password()
    password_hash = pbkdf2_sha256.hash(faker.password())

    assert not validate_password(password=password, password_hash=password_hash)


def test_validate_password_error_password_hash(faker):
    password = faker.password()
    password_hash = faker.uuid4()

    assert not validate_password(password=password, password_hash=password_hash)


def test_validate_password_valid_password(faker):
    password = faker.password()
    password_hash = pbkdf2_sha256.hash(password)

    assert validate_password(password=password, password_hash=password_hash)


def test_generate_password_hash(faker):
    password = faker.password()
    password_hash = generate_password_hash(password=password)

    assert validate_password(password=password, password_hash=password_hash)


def test_generate_token_success(faker, private_key, public_key):
    refresh_token = faker.random_number()
    user_id = faker.random_number()

    some_field = faker.word()
    some_value = faker.word()

    user_data_token = UserDataToken({
        'sub': user_id,
        'jti': None,
        'exp': None,
        some_field: some_value
    })

    token_lifetime = datetime.utcnow() + jwt_identity_settings.JWT_IDENTITY_EXP_TIME
    token = generate_token(user_data_token=user_data_token,
                           refresh_token=refresh_token,
                           private_key=private_key)

    try:
        res = validate_token(token=token, public_key=public_key)  # type: UserDataToken

        assert res.sub == user_id
        assert res.exp == timegm(token_lifetime.utctimetuple())
        assert res.jti == refresh_token
        assert res.get(some_field) == some_value

    except jwt_auth_exceptions.AuthFailedDecodeError as err:
        pytest.fail(f'Exception is raised when we do not wait it: {str(err)}')


def test_update_token_success(private_key, public_key):
    user_data_token = UserDataToken({
        'exp': 0,
        'sub': 0,
        'jti': 0
    })

    token_lifetime = datetime.utcnow() + jwt_identity_settings.JWT_IDENTITY_EXP_TIME
    token = update_token(user_data_token=user_data_token, private_key=private_key)

    try:
        user_data_token = validate_token(token=token, public_key=public_key)  # type: UserDataToken
        assert user_data_token.exp == timegm(token_lifetime.utctimetuple())

    except jwt_auth_exceptions.AuthFailedDecodeError as err:
        pytest.fail(f'Exception is raised when we do not wait it: {str(err)}')
